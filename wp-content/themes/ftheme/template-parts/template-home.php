<?php
/* Template Name: Home */
get_header(); ?>
<?php headerHomePage() ?>
    <main>
		<?php include( get_template_directory() . '/inc/_partials/_home/_about.php' ); ?>
		<?php include( get_template_directory() . '/inc/_partials/_home/_news.php' ); ?>
		<?php include( get_template_directory() . '/inc/_partials/_home/_events.php' ); ?>
		<?php include( get_template_directory() . '/inc/_partials/_home/_panel.php' ); ?>
    </main>
<?php
get_footer();
