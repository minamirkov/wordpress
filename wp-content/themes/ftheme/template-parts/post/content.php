<?php
/**
 * Template Name: Single Post Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ftheme
 */
?>
<?php
$post_second_title    = get_field( 'post_second_title' );
$post_first_paragraph = get_field( 'post_first_paragraph' );
?>
<div class="wrapper m-content">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >

		<?php
		if ( 'post' === get_post_type() ) : ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
			$image       = $image[0]; ?>
		<?php else :
			$image = get_bloginfo( 'stylesheet_directory' ) . '/lib/images/home1-1024x681.jpg'; ?>
		<?php endif; ?>
        <div class="m-content__feature" style="background-image: url('<?php echo $image; ?>')">


            <div class="_wr">
                <div class="_w">
					<?php
					the_title( '<h1 class="_8 m-content__heading">', '</h1>' ); ?>
                </div>
            </div>
        </div>
        <div class="_wr">
            <div class="_w">
                <div class="m-content__posts">
					<?php
					if ( 'post' === get_post_type() ) : ?>
                        <div class="_12 ofs_m1 _m9 ofs_l2 _l7">

                            <p class="m-content__posts--date">    <?php the_date( 'l, F d, Y' ); ?> </p>
                            <h2> <?php echo $post_second_title ?> </h2>
                            <p> <?php echo $post_first_paragraph ?></p>
                        </div>
                        <div class="_12 ofs_m1 _m10 ofs_l2 _l8">
							<?php echo the_content(); ?>
                        </div>

					<?php
					endif; ?>
                    <div class="m-content__social _12 ofs_m1 _m10 ofs_l2 _l8">
                        <span> Social Share </span>
                        <div class="m-content__social--icons">
							<?php
							while ( have_rows( 'socials', 'options' ) ) : the_row();
								$social        = get_sub_field( 'social' );
								$link          = get_sub_field( 'link' );
								$face          = get_sub_field( 'face' );
								$twit          = get_sub_field( 'twit' );
								$you           = get_sub_field( 'you' );
								$medium        = get_sub_field( 'medium' );
								$linkedin_icon = get_sub_field( 'linkedin_icon' );
								$twitter_icon  = get_sub_field( 'twitter_icon' );
								$youtube_icon  = get_sub_field( 'youtube_icon' );
								$facebook_icon = get_sub_field( 'facebook_icon' );
								$medium_icon   = get_sub_field( 'medium_icon' );

								?>
								<?php
								if ( $social == 'Linkedin' ) : ?>
                                    <li>
                                        <a href="<?php echo $link; ?>" target="_blank">
											<?php
											if ( $linkedin_icon ) { ?>
                                                <img src="<?= get_template_directory_uri() . '/bundles/images/d_linkedin.png'; ?>">
												<?php
											} else {
												echo $social;
											} ?>
                                        </a>
                                    </li>
								<?php
								endif;
								if ( $social == 'Facebook' ) : ?>
                                    <li>
                                        <a href="<?php echo $face; ?>" target="_blank">
											<?php
											if ( $facebook_icon ) { ?>
                                                <img src="<?= get_template_directory_uri() . '/bundles/images/d_facebook.png'; ?>">
												<?php
											} else {
												echo $social;
											} ?>
                                        </a>
                                    </li>
								<?php
								endif;
								if ( $social == 'Twitter' ) : ?>
                                    <li>
                                        <a href="<?php echo $twit; ?>" target="_blank">

                                            <img src="<?= get_template_directory_uri() . '/bundles/images/d_twitter.png'; ?>">

                                        </a>
                                    </li>
								<?php
								endif;
								if ( $social == 'Youtube' ) : ?>
                                    <li>
                                        <a href="<?php echo $you; ?>" target="_blank">

                                            <img src="<?= get_template_directory_uri() . '/bundles/images/d_youtube.png'; ?>">

                                        </a>
                                    </li>
								<?php
								endif;
								if ( $social == 'Medium' ) : ?>
                                    <li>
                                        <a href="<?php echo $medium; ?>" target="_blank">
                                            <img src="<?= get_template_directory_uri() . '/bundles/images/d_medium.png'; ?>">
                                        </a>
                                    </li>
								<?php
								endif; ?>
							<?php
							endwhile; ?>
                        </div>
                    </div>
                </div>

                <div class="m-content__links ofs_1 _10">
                    <p class="m-content__links--first"><?php previous_post_link( '%link', 'Previous Post', true ); ?> </p>
                    <p class="m-content__links--second"> <?php next_post_link( '%link', 'Next Post', true ); ?> </p>
                </div>
            </div>

        </div>
    </article>
</div>
