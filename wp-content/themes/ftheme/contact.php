<?php
/* Template Name: Contact Page */
get_header(); ?>
<?php headerPage() ?>
<?php
$contact_title            = get_field( 'contact_title' );
$contact_background_image = get_field( 'contact_background_image' );
$contact_scroll_text      = get_field( 'contact_scroll_text' );
?>
<section class="m-contact" style="background-image:
        url('<?= $contact_background_image['url'] ?>');">
    <div class="m-contact__scroll">
        <a href="#footer" class="m-contact__scroll-button"> <?= $contact_scroll_text ?> </a>
    </div>
    <div class="_wr">
        <div class="_w m-contact__top">
            <h1 class="m-contact__heading _12 ofs_l1 _l7"> <?= $contact_title ?></h1>
            <div class="_12 _m6 ofs_l1 _l3 m-contact__info">

				<?php

				if ( have_rows( 'contact_info_repeater' ) ):

				while ( have_rows( 'contact_info_repeater' ) ) :
					the_row();

					$contact_info_icon             = get_sub_field( 'contact_info_icon' );
					$contact_info_placeholder_text = get_sub_field( 'contact_info_placeholder_text' ); ?>
                    <div class="m-contact__info">
                        <img src="<?php echo $contact_info_icon['url'] ?>" alt="">
                        <span> <?= $contact_info_placeholder_text ?> </span>
                    </div>
				<?php endwhile; ?>
            </div>
			<?php endif; ?>
        </div>

        <div class="_w">
            <div class="m-contact__form _12 ofs_m1 _m8 ofs_l2 _l6">
				<?php echo do_shortcode( '[contact-form-7 id="5" title="Contact form 1"]' ); ?>
            </div>
        </div>
    </div>

</section>

<?php
get_footer(); ?>
