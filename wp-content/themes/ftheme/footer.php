<?php
/**
 * @package ftheme
 */
?>
<?php
$footer_background_image = get_field( 'footer_background_image', 'options' );
$footer_title            = get_field( 'footer_title', 'options' );
$footer_copyright_text   = get_field( 'footer_copyright_text', 'options' );
?>
<footer>

    <div class="m-footer" style="background-image: url('<?= $footer_background_image['url'] ?>');" id="footer">

        <h2> <?= $footer_title ?> </h2>
        <div class="m-footer__navigation">
            <div class="m-footer__menu">
				<?php wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'f-primary-menu',
					'menu_class'     => 'm-footer__items'
				) ); ?>
				<?php include( get_template_directory() . '/inc/_partials/_home/_social.php' ); ?>
            </div>
        </div>
        <div class="m-footer__copyright">
            <div class="m-footer__copyright--left">
                <p><?= $footer_copyright_text ?></p>
            </div>
            <div class="m-footer__copyright--right">
				<?php wp_nav_menu( array(
					'theme_location' => 'secondary',
					'menu_id'        => 'f-secondary',
					'menu_class'     => 'm-footer__copyright--right'
				) ); ?>
            </div>

        </div>

</footer>

<?php wp_footer(); ?>
</body>
</html>
