<? phphttp://localhost:3000/
/**
 * @package ftheme
 */
global $globalSite;
?>
<?php
$header_image = get_field( 'header_image' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php if ( has_site_icon() ): ?>
        <link rel="icon" href="<?php echo get_site_icon_url(); ?>" type="image/x-icon"/>
	<?php else: ?>
        <link rel="icon" href="<?php echo get_template_directory_uri() . '/bundles/images/favicon.ico'; ?>"
              type="image/x-icon"/>
	<?php endif; ?>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
