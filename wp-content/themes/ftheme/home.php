<?php
/* Template Name: Posts Page */
get_header(); ?>
<?php
headerPage(); ?>
<?php
$news_scroll_text      = get_field( 'news_scroll_text', 'option' );
$news_background_image = get_field( 'news_background_image', 'option' );
$become_member_title   = get_field( 'become_member_title', 'option' );
$become_member_text    = get_field( 'become_member_text', 'option' );
?>

<div class="m-blog" style="background-image:
        url('<?php echo $news_background_image; ?> '),
        url(<?= get_template_directory_uri() . '/bundles/images/news-header-bcg.jpg'; ?>);">
    <div class="m-blog__scroll">
        <a href="#footer" class="m-blog__scroll-button"> <?= $news_scroll_text ?> </a>
    </div>
    <div class="_wr m-blog__wrapper">
    <div class="_w">
<?php if ( is_home() ) : ?>

    <h1 class="m-blog__title"><?php single_post_title(); ?></h1>
    <div class="_12 _m11 _l8 m-blog__left">
        <div class="swiper m-blog__swiper">
            <div class="swiper-wrapper">
				<?php if ( have_posts() ): ?>
					<?php while ( have_posts() ): ?>
						<?php the_post(); ?>
						<?php if ( has_post_thumbnail() ) :
							$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ); ?>
                            <div class="swiper-slide m-blog__slide"
                                 style="background-image: url('<?php echo $featured_image[0]; ?>')">
                                <div class="m-blog__slideText">
                                    <span> <?php the_date( 'l, F d, Y' ) ?></span>
                                    <a href="<?php the_permalink(); ?>"><h4> <?php the_title(); ?> </h4></a>
                                    <p><?php the_excerpt(); ?> </p>
                                </div>

                            </div>
						<?php endif; ?>

					<?php endwhile; ?>
				<?php else: ?>
                    <p><?php _e( 'No Blog Posts found', 'nd_dosth' ); ?></p>
				<?php endif; ?>

            </div>
            <div class="swiper-pagination"></div>
        </div>

        <div class="m-blog__posts">
			<?php if ( have_posts() ): ?>
				<?php while ( have_posts() ): ?>
                    <div class="m-blog__post">
						<?php the_post(); ?>

						<?php if ( has_post_thumbnail() ) :
							$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ); ?>
                            <div class="m-blog__post--image">
                                <a href="<?php the_permalink(); ?>"><img src="<?php echo $featured_image[0]; ?>"
                                                                         alt=''/></a>
                            </div>
                            <div class="m-blog__post--text">
                                <span> <?php the_date( 'l, F d, Y' ) ?></span>
                                <a href="<?php the_permalink(); ?>"><h4> <?php the_title(); ?> </h4></a>
                                <a class="m-blog__post--link"
                                   href="<?php the_permalink(); ?>"><?php _e( 'Read More' ); ?> </a>
                            </div>
						<?php endif; ?>


                    </div>
				<?php endwhile; ?>
			<?php else: ?>
                <p><?php _e( 'No Blog Posts found', 'nd_dosth' ); ?></p>
			<?php endif; ?>
        </div>
    </div>

    </div>
    </div>
    <div class=" m-blog__sidebar">
        <div class="m-blog__sidebarWp">
            <h4> Categories </h4>
			<?php
			$categories = get_categories( array(
				'hide_empty' => false,
			) );
			foreach ( $categories as $category ) {
				echo '<div class="m-blog__sidebar--categories"> <a href="' . get_category_link( $category->term_id ) . '">' . $category->name . '</a> </div>';
			}
			?>
            <h4 class="m-blog__sidebar--heading"> Most Popular</h4>

            <div class="m-blog__sidebar--search">
                <div class="m-blog__sidebar--label">
                    <label for=""> Search for keywords...
                    </label>
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-search" width="20"
                         height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" fill="none"
                         stroke-linecap="round"
                         stroke-linejoin="round">
                        <path d="M0 0h24v24H0z" stroke="none"/>
                        <circle cx="10" cy="10" r="7"/>
                        <path d="m21 21-6-6"/>
                    </svg>
                </div>


				<?php get_search_form() ?> </div>

			<?php
			$the_query = new WP_Query( 'posts_per_page=5' ); ?>
			<?php
			while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
                <div class="m-blog__sidebar--posts">
                    <p><?php echo get_the_date( "l, F d, Y" ); ?> </p>
                    <h5><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
                </div>
			<?php endwhile;
			wp_reset_postdata();
			?>
        </div>
        <div class="m-blog__member ">
            <h5> <?= $become_member_title ?> </h5>
            <p> <?= $become_member_text ?> </p>
			<?php
			$become_member_button            = get_field( 'become_member_button', 'option' );
			if ( $become_member_button ):
				$become_member_button_url = $become_member_button['url'];
				$become_member_button_title  = $become_member_button['title'];
				$become_member_button_target = $become_member_button['target'] ? $become_member_button['target'] : '_self';
				?>
                <a class="a-button -primary" href="<?php echo esc_url( $become_member_button_url ); ?>"
                   target="<?php echo esc_attr( $become_member_button_target ); ?>"><?php echo esc_html( $become_member_button_title ); ?></a>
			<?php endif; ?>
        </div>

    </div>

    </div>

<?php endif; ?>
<?php
// get_sidebar();
get_footer();