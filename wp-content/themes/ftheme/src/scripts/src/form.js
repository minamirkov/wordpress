document.addEventListener('DOMContentLoaded', function () {
    var inputs = document.querySelectorAll('.form-group input, .form-group textarea');
    inputs.forEach(input => {

        input.addEventListener('focus', (e) => {
            e.target.closest('.form-group').classList.add('has-value');
        });

        input.addEventListener('blur', (e) => {

            if (input.value.length === 0) {
                e.target.closest('.form-group').classList.remove('has-value');

            } else {
                e.target.closest('.form-group').classList.add('has-value');
            }
        });
    });
});