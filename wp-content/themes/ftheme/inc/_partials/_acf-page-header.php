<?php

/**
 * Header for pages
 * Custom header image functionality
 * @package WordPress
 */

/**
 * @return mixed|string in case there is a image array in field, returns random image from it, otherwise returns random.jpg from images
 *
 */
function randomHeaderImage() {

	$images = array();

	if ( have_rows( 'imgs', 'options' ) ) :
		while ( have_rows( 'imgs', 'options' ) ) : the_row();
			$image = get_sub_field( 'img' );

			array_push( $images, $image );
		endwhile;
		$random_counter = rand( 0, count( $images ) - 1 );

	endif;
	if ( ! empty( $images ) ) {
		return $images[ $random_counter ];
	} else {
		return get_template_directory_uri() . '/src/images/random.jpg';
	}
}

/**
 * Custom header for home page
 * Change name of images that show up by default if nothing is selected (from random.jpg)
 */
function headerHomePage() {
	$header_image = get_field( 'header_image', 'options' );
	$header_title = get_field( 'header_title', 'options' );
	$image        = $header_image;

	$title                        = get_ftheme_first( [ $header_title, get_the_title(), get_the_archive_title() ] );
	$header_hero_scroll_text      = get_field( 'header_hero_scroll_text' );
	$header_hero_heading          = get_field( 'header_hero_heading' );
	$header_hero_paragraph        = get_field( 'header_hero_paragraph' );
	$header_hero_button_text      = get_field( 'header_hero_button_text' );
	$header_hero_background_image = get_field( 'header_hero_background_image' );
	$header_hero_scroll_text      = get_field( 'header_hero_scroll_text' );
	$header_hero_button           = get_field( 'header_hero_button' );
	$header_hero_button_url       = $header_hero_button['url'];
	$header_hero_button_title     = $header_hero_button['title'];
	$header_hero_button_target    = $header_hero_button['target'] ? $header_hero_button['target'] : '_self';
	?>

    <header class="m-header" style="background-image:
            url(<?= get_template_directory_uri() . "/bundles/images/lines-bkg.png" ?>) ,
            url('<?= $header_hero_background_image ?>');">
        <div class="m-overlay js-navOverlay"></div>
        <nav class="m-header__navigation">
            <div class="m-header__logo">
                <a href="<?php _e( home_url( '/' ) ); ?>">
					<?php
					if ( $header_image ) :
						?>
                        <img src="<?php echo $header_image ?>" alt="">
					<?php
					else :
						?>
                        <img src="<?= get_template_directory_uri() . '/bundles/images/logo.png'; ?>" alt="">
					<?php endif; ?>
                </a>
            </div>
            <button class="m-header__button js-toggleNav">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div class="m-header__navMenu">
				<?php wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'menu_class'     => 'menu-items'
				) ); ?>
            </div>
        </nav>
        <div class="m-hero">
            <div class="m-hero__scroll">
                <a href="#about" class="m-hero__scroll-button"> <?= $header_hero_scroll_text ?> </a>
            </div>
            <div class="_wr m-hero__text">
                <div class="_w ">
                    <div class="_12 ofs_m1 _m9 ofs_l1 _l5">
                        <h1> <?php echo $header_hero_heading ?> </h1>
                        <p> <?php echo $header_hero_paragraph ?> </p>
						<?php
						if ( $header_hero_button ) :
							?>
                            <!-- podseti me da ti pokazem kako da kondicijalno ubacujes sve ove atribute da izbegnes error-e u slucaju da user ne ubaci tu informaciju u dashboard -->
                            <a class="a-button -secondary -hpt40 -hpb96"
                               href="<?php echo esc_url( $header_hero_button_url ); ?>"
                               target="<?php echo esc_attr( $header_hero_button_target ); ?>"><?php echo esc_html( $header_hero_button_title ); ?>
                                <div class="icon">
                                    <div class="arrow"></div>
                                </div>
                            </a>
						<?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="m-hero__social">
				<?php include( get_template_directory() . '/inc/_partials/_home/_social.php' ); ?>
            </div>
        </div>
        <div class="m-info">
            <div class="wr">
                <div class="_w">
					<?php

					if ( have_rows( 'info_repeater' ) ) :
						?>
						<?php
						while ( have_rows( 'info_repeater' ) ) :
							the_row();
							$info_repeater_text  = get_sub_field( 'info_repeater_text' );
							$info_repeater_image = get_sub_field( 'info_repeater_image' );
							?>
                            <div class="_12 _m6 _l4 m-info__content">
                                <img src="<?= $info_repeater_image['url'] ?>" alt="">
                                <h4><?= $info_repeater_text ?></h4>
                            </div>
						<?php
						endwhile;
						?>

					<?php
					endif;
					?>
                </div>
            </div>
        </div>
    </header>
	<?php
}

/**
 * Custom header for all other pages
 * Change name of images that show up by default if nothing is selected (from random.jpg)
 */
function headerPage() {
	$header_image = get_field( 'header_image', 'options' );

	?>
    <header class="m-header">
    <div class="m-overlay js-navOverlay"></div>
    <nav class="m-header__navigation">
        <div class="m-header__logo">
            <a href="<?php _e( home_url( '/' ) ); ?>">
				<?php
				if ( $header_image ) :
					?>
                    <img src="<?php echo $header_image ?>" alt="">
				<?php
				else :
					?>
                    <img src="<?= get_template_directory_uri() . '/bundles/images/logo.png'; ?>" alt="">
				<?php endif; ?>
            </a>
        </div>
        <button class="m-header__button js-toggleNav">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="m-header__navMenu">
			<?php wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
				'menu_class'     => 'menu-items'
			) ); ?>
        </div>
    </nav>


	<?php
}
