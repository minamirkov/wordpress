<?php
$about_background_image = get_field( 'about_background_image' );
$about_short_title      = get_field( 'about_short_title' );
$about_main_title       = get_field( 'about_main_title' );
$about_first_excerpt    = get_field( 'about_first_excerpt' );
$about_second_excerpt   = get_field( 'about_second_excerpt' );
?>
<section class="m-about" id="about" style="background-image:
        url(<?= get_template_directory_uri() . "/bundles/images/lines-bkg.png" ?>);">
    <div class="js-IframeOverlay m-about__overlay"></div>
    <div class="_wr">
        <div class="_w">
            <div class="m-about__text _12 _l6">

                <p class="m-about__greenTxt"> <?= $about_short_title ?> </p>
                <h2> <?php echo $about_main_title ?> </h2>
                <!-- jednostavnije je da nazoves ovo samo text a ne excerpt jer je excerpt u principu odlomak nekog teksta -->
                <p class="m-about__mainExc"> <?php echo $about_first_excerpt ?> </p>

                <div class="m-about__right">
                    <div class="m-about__secExc">
                        <p> <?php echo $about_second_excerpt ?> </p>
						<?php
						$about_button        = get_field( 'about_button' );
						$about_button_url    = $about_button['url'];
						$about_button_title  = $about_button['title'];
						$about_button_target = $about_button['target'] ? $about_button['target'] : '_self';
						if ( $about_button ) :

							?>
                            <a class="js-toggleIframe m-about__button">
                                <div class="m-about__button--circle">
                                    <div class="m-about__button--triangle"></div>
                                </div>
                                <span class="m-about__button--title"> <?php echo esc_html( $about_button_title ); ?> </span>
                            </a>
						<?php endif; ?>
                    </div>
                </div>


            </div>
            <div class="m-about__image _12 _l6">
                <img src="<?= $about_background_image['url'] ?>" alt="">
            </div>
        </div>
    </div>
    <div class="m-about__iframe">

        <iframe src="https://www.youtube.com/embed/41JCpzvnn_0"
                title="YouTube video player"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen>
        </iframe>
        <div class="m-about__iframe--span js-toggleIframe">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</section>