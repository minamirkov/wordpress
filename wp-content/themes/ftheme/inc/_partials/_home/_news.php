<?php
$news_title         = get_field( 'news_title' );
$news_button        = get_field( 'news_button' );
$news_button_url    = $news_button['url'];
$news_button_title  = $news_button['title'];
$news_button_target = $news_button['target'] ? $news_button['target'] : '_self';
?>
<section class="m-news">
    <div class="_wr">
        <div class="_w m-news__top">
            <h2> <?php echo $news_title ?> </h2>
            <div class="a-button -secondary">
				<?php
				if ( $news_button ):
					?>
                    <a href="<?php echo esc_url( $news_button_url ); ?>"
                       target="<?php echo esc_attr( $news_button_target ); ?>"><?php echo esc_html( $news_button_title ); ?></a>
				<?php endif; ?>
            </div>
        </div>
    </div>
    <div class="_wr m-news__posts">
        <div class="_w">
            <div class="m-news__post _12 _l5">
				<?php
				$args      = [
					'posts_per_page' => 1,
					'post_type'      => 'post',

				];
				$the_query = new WP_Query( $args ); ?>
				<?php
				while ( $the_query->have_posts() ) :
					$the_query->the_post();
					?>
                    <article>
                    <div class="m-news__post-image">
					<?php if ( has_post_thumbnail() ) :
					$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ); ?>
                    <a href="<?php the_permalink(); ?>"><img class="m-posts__img"
                                                             src="<?php echo $featured_image[0]; ?>"
                                                             alt=''/></a>
                    </div>
                    <div class="m-news__post--text">
                        <span class="m-news__post--date"><?php echo get_the_date( "l, F d, Y" ); ?> </span>
                        <h5>
                            <a href="<?php the_permalink() ?>" class="m-news__post--title"><?php the_title(); ?></a>
                        </h5>
						<?php the_excerpt(); ?>
                        <a href="<?php the_permalink() ?>" class="m-news__post--link"> Read more </a>
                    </div>
                    </article>
				<?php endif; ?>
				<?php endwhile;
				wp_reset_postdata();
				?>
            </div>
            <div class="m-news__post2 _12 _l7">

				<?php
				$args_second      = [
					'posts_per_page' => 2,
					'offset'         => '1',
					'post_type'      => 'post',

				];
				$the_query_second = new WP_Query( $args_second ); ?>
				<?php
				while ( $the_query_second->have_posts() ) :
					$the_query_second->the_post();
					?>
                    <article>
                    <div class="m-news__post2-image">
					<?php if ( has_post_thumbnail() ) :
					$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ); ?>
                    <a href="<?php the_permalink(); ?>"><img class="m-posts__img"
                                                             src="<?php echo $featured_image[0]; ?>"
                                                             alt=''/></a>
                    </div>
                    <div class="m-news__post2--text">
                        <span class="m-news__post2--date"><?php echo get_the_date( "l, F d, Y" ); ?> </span>
                        <h5>
                            <a href="<?php the_permalink() ?>" class="m-news__post--title"><?php the_title(); ?></a>
                        </h5>
                        <a href="<?php the_permalink() ?>" class="m-news__post2--link"> Read more </a>
                    </div>
                    </article>
				<?php endif; ?>
				<?php endwhile;
				wp_reset_postdata();
				?>
            </div>
        </div>
    </div>
</section>

