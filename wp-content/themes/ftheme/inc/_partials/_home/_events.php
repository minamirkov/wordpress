<?php
$events_title            = get_field( 'events_title' );
$events_text             = get_field( 'events_text' );
$events_button           = get_field( 'events_button' );
$events_background_image = get_field( 'events_background_image' );

$panel_image = get_field( 'panel_image' );
$panel_title = get_field( 'panel_title' );
$panel_text  = get_field( 'panel_text' );


?>
<section class="m-events" style="background-image: url('<?= $events_background_image['url'] ?>');">
    <div class="_wr ">
        <div class="_w m-events__top">
            <h2> <?= $events_title ?> </h2>


            <p class="m-events__top--text"> <?= $events_text ?> </p>
            <div class="a-button -secondary">
				<?php
				$events_button_url    = $events_button['url'];
				$events_button_title  = $events_button['title'];
				$events_button_target = $events_button['target'] ? $events_button['target'] : '_self';
				if ( $events_button ):
					?>
                    <a href="<?php echo esc_url( $events_button_url ); ?>"
                       target="<?php echo esc_attr( $events_button_target ); ?>"><?php echo esc_html( $events_button_title ); ?></a>
				<?php endif; ?>
            </div>
        </div>
    </div>

    <div class="swiper m-events__swiper">
        <div class="swiper-wrapper m-events__swiperWrapper">
			<?php
			if ( have_rows( 'events_repeater' ) ):
				?>
				<?php
				while ( have_rows( 'events_repeater' ) ) :
					the_row();
					$events_repeater_text             = get_sub_field( 'events_repeater_text' );
					$events_repeater_background_image = get_sub_field( 'events_repeater_background_image' );
					$events_repeater_date             = get_sub_field( 'events_repeater_date' );
					?>
                    <div class="swiper-slide m-events__swiperSlide"
                         style="background-image: linear-gradient(180deg, rgba(37, 37, 37, 0) 0%, #252525 100%), url('<?= $events_repeater_background_image['url'] ?>')">
                        <span> <?= $events_repeater_date ?></span>
                        <h5><?= $events_repeater_text ?> </h5>
                    </div>
				<?php
				endwhile;
				?>
			<?php
			endif;
			?>
        </div>
        <div class="m-events__swiper--button">
            <div class="swiper-button-prev m-events__swiper--buttonPrev">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-arrow-narrow-left"
                     width="35"
                     height="35" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" fill="none" stroke-linecap="round"
                     stroke-linejoin="round">
                    <path d="M0 0h24v24H0z" stroke="none"/>
                    <path d="M5 12h14M5 12l4 4M5 12l4-4"/>
                </svg>
            </div>
            <div class="swiper-button-next m-events__swiper--buttonNext">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-arrow-narrow-right"
                     width="35"
                     height="35" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" fill="none" stroke-linecap="round"
                     stroke-linejoin="round">
                    <path d="M0 0h24v24H0z" stroke="none"/>
                    <path d="M5 12h14M15 16l4-4M15 8l4 4"/>
                </svg>
            </div>
        </div>
    </div>
    <div class="m-panel">
        <div class="_wr">
            <div class="_w m-panel__center">
                <div class="_12 _m6 _l3 m-panel__image">
                    <img src="<?php echo $panel_image['url'] ?>" alt="">
                </div>
                <div class="_12 _m6 _l4 m-panel__center--text">
                    <h3> <?php echo $panel_title ?> </h3>
                    <p> <?php echo $panel_text ?> </p>
                </div>
                <div class="_12 _m12 _l5 m-panel__center">
					<?php echo do_shortcode( '[contact-form-7 id="01" title="Become a member"]' ); ?>
                </div>
            </div>
        </div>
    </div>
</section>