<div class="m-social">
    <ul>
		<?php
		while ( have_rows( 'socials', 'options' ) ) :
			the_row();
			$social        = get_sub_field( 'social' );
			$link          = get_sub_field( 'link' );
			$face          = get_sub_field( 'face' );
			$twit          = get_sub_field( 'twit' );
			$you           = get_sub_field( 'you' );
			$medium        = get_sub_field( 'medium' );
			$linkedin_icon = get_sub_field( 'linkedin_icon' );
			$twitter_icon  = get_sub_field( 'twitter_icon' );
			$youtube_icon  = get_sub_field( 'youtube_icon' );
			$facebook_icon = get_sub_field( 'facebook_icon' );
			$medium_icon   = get_sub_field( 'medium_icon' );

			?>

			<?php
			if ( $social == 'Linkedin' ) : ?>
                <li>
                    <a href="<?php echo $link; ?>" target="_blank">
						<?php
						if ( $linkedin_icon ) { ?>
                            <img src="<?= get_template_directory_uri() . '/bundles/images/linkedin.png'; ?>">
							<?php
						} else {
							echo $social;
						} ?>
                    </a>
                </li>
			<?php
			endif;
			if ( $social == 'Facebook' ) : ?>
                <li>
                    <a href="<?php echo $face; ?>" target="_blank">
						<?php
						if ( $facebook_icon ) { ?>
                            <img src="<?= get_template_directory_uri() . '/bundles/images/facebook.png'; ?>">
							<?php
						} else {
							echo $social;
						} ?>
                    </a>
                </li>
			<?php
			endif;
			if ( $social == 'Twitter' ) : ?>
                <li>
                    <a href="<?php echo $twit; ?>" target="_blank">

                        <img src="<?= get_template_directory_uri() . '/bundles/images/twitter.png'; ?>">

                    </a>
                </li>
			<?php
			endif;
			if ( $social == 'Youtube' ) : ?>
                <li>
                    <a href="<?php echo $you; ?>" target="_blank">

                        <img src="<?= get_template_directory_uri() . '/bundles/images/youtube.png'; ?>">

                    </a>
                </li>
			<?php
			endif;
			if ( $social == 'Medium' ) : ?>
                <li>
                    <a href="<?php echo $medium; ?>" target="_blank">
                        <img src="<?= get_template_directory_uri() . '/bundles/images/medium.png'; ?>">
                    </a>
                </li>
			<?php
			endif; ?>
		<?php
		endwhile; ?>
    </ul>
</div>